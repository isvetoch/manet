/**
  ******************************************************************************
  * @file    sslip.c
  * @author  Andrew Gazizov
  * @version V1.0.0
  * @date    25/11/2014
  * @brief   Provides routines for processing input chip serial data
  ******************************************************************************
**/

/* Includes ------------------------------------------------------------------*/
/* Standard includes. */
#include <stdio.h>

/* Library includes. */

/* Headers includes. */
#include "sslip.h"
#include "misc.h"

/* Macros --------------------------------------------------------------------*/

/* Variables -----------------------------------------------------------------*/

/* Function Prototypes -------------------------------------------------------*/

/**
  * @brief	sslip_find_data_pos(byte *data, u8 len, byte cmd)
  * 		========== Find data position in string
  * @param  *data: byte[x]
  * 	- Input data
  * @param  len: {0-255}
  * 	- Data length
  * @param  cmd: {0-0xFF}
  * 	- Chip command
  * @retval return: {0-255}
  * 	- Index of byte array where data begins
*/
u8 sslip_find_data_pos(byte *data, u8 len, byte cmd) {
	u8 i = 0;

	while (i < len) {
		if (data[i] == 'c') {
			i++;
			if (data[i] == 'm') {
				i++;
				if (data[i] == 'd') {
					i++;
					if (data[i] == cmd) {
						i++;
						return i;
					} else continue;
				} else continue;
			} else continue;
		} else i++;
	}
	return 0xFF;
}

/**
  * @brief	unsigned short sslip_find_cmd_pos(byte *data, u8 len)
  * 		========== Find command position in string
  * @param  *data: byte[x]
  * 	- Input data
  * @param  len: {0-255}
  * 	- Data length
  * @retval return: {0-255}
  * 	- Index of byte array where command begins
*/
unsigned short sslip_find_cmd_pos(byte *data, u8 len) {
	u8 i = 0;

	while (i < len) {
		if (data[i] == 'c') {
			i++;
			if (data[i] == 'm') {
				i++;
				if (data[i] == 'd') {
					i++;
					return i;
				} else continue;
			} else continue;
		} else i++;
	}
	return 0xFFFF;
}


