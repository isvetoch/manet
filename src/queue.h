/**
  ******************************************************************************
  * @file    queue.h
  * @author  Andrew Gazizov
  * @version V1.0.0
  * @date    14/05/2015
  * @brief   This file contains global variables and function prototypes for
  * 		 queue
  ******************************************************************************
**/

#ifndef QUEUE_H_
#define QUEUE_H_

/* Includes ------------------------------------------------------------------*/
/* Standard includes. */
/* Library includes. */
/* Headers includes. */

/* Macros --------------------------------------------------------------------*/

/* Function Prototypes --------------------------------------------------------*/
void Queue_init(void);
void Limbo_clean(time_t);
void Queue_clean(time_t);
bool Queue_empty(void);
void Queue_push(bool, byte *, byte *, byte *, u8);
void Limbo_push(bool, byte *, byte *, byte *, u8, byte *);
void Queue_pull(bool *, byte *, byte *, byte *, u8 *);
void Limbo_pull(byte *);

#endif /* QUEUE_H_ */
