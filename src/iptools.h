/**
  ******************************************************************************
  * @file    iptools.h
  * @author  Andrew Gazizov
  * @version V1.0.0
  * @date    23/11/2014
  * @brief   This file contains global variables and function prototypes for IP
  ******************************************************************************
**/

#ifndef IPTOOLS_H_
#define IPTOOLS_H_

/* Includes ------------------------------------------------------------------*/
/* Standard includes. */
#include "misc.h"

/* Library includes. */

/* Headers includes. */

/* Macros ---------------------------------------------------------------------*/

/* Global Variables -----------------------------------------------------------*/
byte SOME_LOCAL_IP[16];
byte LOCAL_PREFIX[8];
byte BROADCAST16[8];
byte BROADCAST_IP[16];

/* Function Prototypes --------------------------------------------------------*/
void iptools_init(void);
unsigned char ip6_tc();
void invert_LU_bit(byte *);
void eui_to_ip(byte *, byte *);
bool is_broadcast(byte *);
void get_l2(byte *, byte *);
void l2_to_ip(byte *, byte *, byte *);
void ip6_init(byte *);
unsigned char ip6_v();
void ip6_v_set(unsigned char);
unsigned int ip6_flow();
void ip6_flow_set(unsigned int);
unsigned char ip6_tc();
unsigned char ip6_tc_set(unsigned char);
bool ip6_is_udp();
void ip6_short_fill(byte *);
bool l2_is_short(byte *);
void UDP_init(byte *);
void ip6_pack(byte *);


// This struct contains IPv6 packet
struct ip6_struct {
	unsigned int v_tc_flow;
	unsigned short plen;
	unsigned char nxt;
	unsigned char hlim;
	byte src[16];
	byte dst[16];
	byte data[2048];
} ip6;

// This struct contains UDP packet
struct udp_struct {
	unsigned short sport;
	unsigned short dport;
	unsigned short ulen;
	unsigned short sum;
	byte data[2048];
} udp;

#endif /* IPTOOLS_H_ */
