/**
  ******************************************************************************
  * @file    manet.c
  * @author  Andrew Gazizov
  * @version V1.0.0
  * @date    22/11/2014
  * @brief   Main program entry point. Gets from user proper info, configures
  * 			all systems and starts main network program.
  ******************************************************************************
**/

/* Includes ------------------------------------------------------------------*/
/* Standard includes. */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <signal.h>
#include <popt.h>

/* Library includes. */

/* Headers includes. */
#include "manet.h"
#include "iptools.h"
#include "node.h"
#include "chip_master.h"
#include "binascii.h"
#include "logger.h"

/* Macros --------------------------------------------------------------------*/
#define EXTERNAL 	0
#define INTERNAL 	1

#define VERSION		"V5.9.29"

//#define SETPIDFILE

/* Variables -----------------------------------------------------------------*/

/* Function Prototypes -------------------------------------------------------*/

/**
  * @brief	static void manet_on_terminate(int sig, siginfo_t *siginfo, void *context)
  * 		========== Application terminate handler
  * @param  sig: int
  * @param  *siginfo: siginfo_t
  * @param  *context: void
  * @retval none
*/
static void manet_on_terminate(int sig, siginfo_t *siginfo, void *context)
{
	puts("Terminating app");
	exit(0);
}

//int on_exit(void (*function)(int , void *), void *arg) {
//	puts("on exit");
//}

/**
  * @brief	void OptionParser(int argc, char *argv[])
  * 		========== Stores console input program options
  * @param  argc: {0-10}
  * 	- Number of input option strings
  * @param	*argv: char[x]
  * 	- Array of input strings
  * @retval none
*/
void setPidFile(const char *filename)
{
    FILE *f;

    f = fopen(filename, "w+");
    if (f)
    {
        fprintf(f, "%u", getpid());
        fclose(f);
    }
}

void exit_handler(int ev, void *arg) {
	puts("exit handler");
}

/**
  * @brief	u8 set_sighandler(void)
  * 		========== Sets system signal handlers
  * @param  none
  * @retval none
*/
 u8 set_sighandler(void)
{
	struct sigaction act_on_terminate;
	int ret;
	FILE *out;

	memset (&act_on_terminate, '\0', sizeof(act_on_terminate));
	/* Use the sa_sigaction field because the handles has two additional parameters */
	act_on_terminate.sa_sigaction = &manet_on_terminate;
	/* The SA_SIGINFO flag tells sigaction() to use the sa_sigaction field, not sa_handler. */
	act_on_terminate.sa_flags = SA_SIGINFO;

	ret = sigaction(SIGTSTP, &act_on_terminate, NULL);
	if (ret < 0) {
		perror ("sigaction");
		return ret;
	}

	ret = sigaction(SIGINT, &act_on_terminate, NULL);
	if (ret < 0) {
		perror ("sigaction");
		return ret;
	}

	on_exit(exit_handler, out);
	return 0;
}

/**
  * @brief	void OptionParser(int argc, char *argv[])
  * 		========== Stores console input program options
  * @param  argc: {0-10}
  * 	- Number of input option strings
  * @param	*argv: char[x]
  * 	- Array of input strings
  * @retval none
*/
void OptionParser(int argc, char *argv[]) {
	poptContext optCon;

	cfg.baud = 460800;
	cfg.power = 100;
	cfg.channel = 5;
	cfg.lan = false;
	cfg.dbus = false;
	cfg.node_status = RUN;
	__g_loglevel = DEBUG;

	struct poptOption optionsTable[] = {
			// Tunnel settings
			{ "iface", 'i', POPT_ARG_STRING, &cfg.iface, 0, "tun interface name" },
			{ "lan", '\0', POPT_ARG_NONE, &cfg.lan, 0, "use ethernet LAN network for some radio nodes" },
			{ "lan_iface", '\0', POPT_ARG_STRING, &cfg.lan_iface, 0, "interface name of ethernet LAN network" },
			// Serial port settings
			{ "port", 'p', POPT_ARG_STRING, &cfg.port, 0, "serial port file. default: /dev/ttyUSB0" },
			{ "baud", 'b', POPT_ARG_INT, &cfg.baud, 0, "baudrate: 230400, 460800 or 921600" },
			// Radio chip settings
			{ "chip", 'c', POPT_ARG_STRING, &cfg.chip, 0, "radio chip model: cc430 or stm32w" },
			{ "antenna", 'a', POPT_ARG_STRING, &cfg.chip, 0, "type of antenna: external or internal (for stm32w only)" },
			{ "power", 'o', POPT_ARG_INT, &cfg.power, 0, "transmit power 0...100, default: max" },
			{ "channel", '\0', POPT_ARG_INT, &cfg.channel, 0, "radio channel (0 .. 46 for cc430), (0 .. 15 for stm32w)" },
			// Log settings
			{ "loglevel", 'l', POPT_ARG_STRING, &cfg.log_level, 0, "log level: DEBUG, INFO, ERROR, SILENT" },
			POPT_AUTOHELP
			{NULL, '\0', POPT_ARG_NONE, NULL, 0, NULL, NULL}
	};
	optCon = poptGetContext("manet", argc, argv, optionsTable, 0);
	poptGetNextOpt(optCon);
	cfg.channel = 5;		// FIXME

	if (cfg.port == NULL) {
		cfg.port = malloc(16);
		strcpy(cfg.port, "/dev/ttyUSB0");
	}
	if (cfg.iface == NULL) {
		cfg.iface = malloc(8);
		strcpy(cfg.iface, "radio0");
	}
	if (cfg.antenna == NULL) {
		cfg.antenna = malloc(16);
		strcpy(cfg.antenna, "internal");
	}
	if (cfg.chip == NULL) {
		cfg.chip = malloc(8);
		strcpy(cfg.chip, "stm32w");
	}

	if (cfg.log_level == NULL) {
		cfg.log_level = malloc(8);
		strcpy(cfg.log_level, "ERROR");
	}

	if (!strcmp(cfg.log_level, "SILENT"))
		__g_loglevel = SILENT;
	else if (!strcmp(cfg.log_level, "ERROR"))
		__g_loglevel = ERROR;
	else if (!strcmp(cfg.log_level, "WARNING"))
		__g_loglevel = WARNING;
	else if (!strcmp(cfg.log_level, "INFO"))
		__g_loglevel = INFO;
	else if (!strcmp(cfg.log_level, "DEBUG"))
		__g_loglevel = DEBUG;

	LOG_TRACE(INFO,"Device params:");
	LOG_TRACE(INFO, "IFACE: %s", cfg.iface);
	LOG_TRACE(INFO, "PORT: %s", cfg.port);
	LOG_TRACE(INFO, "BAUD: %i", (int) cfg.baud);

	LOG_TRACE(INFO, "CHIP: %s", cfg.chip);
	LOG_TRACE(INFO, "ANT: %s", cfg.antenna);
	LOG_TRACE(INFO, "PWR: %i", cfg.power);
	LOG_TRACE(INFO, "CH: %i\n", cfg.channel);

	if (cfg.lan) {
		if (cfg.lan_iface != NULL)
			LOG_TRACE(INFO, "Use LAN via %s", cfg.lan_iface);
		else
			LOG_TRACE(INFO, "Use LAN via ALL interfaces\n");
	}

	poptFreeContext(optCon);
}

/**
  * @brief	int main(int argc, char *argv[])
  * 		========== Manet program entry point
  * @param  argc: {0-10}
  * 	- Number of console input option strings
  * @param	*argv: char[x]
  * 	- Array of console input strings
  * @retval none
*/
int main(int argc, char *argv[]) {
	printf("Manet application version: %s\n", VERSION);
	log_init();
	OptionParser(argc, argv);

#ifdef SETPIDFILE
	setPidFile("/var/run/manet.pid");
#endif

	if (set_sighandler() < 0)
		LOG_TRACE(ERROR, "Failed to set signal handlers.");

	iptools_init();
	Node_Init();
	node_run();

	return 0;
}
