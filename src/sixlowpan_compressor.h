/**
  ******************************************************************************
  * @file    sixlowpan_compressor.h
  * @author  Andrew Gazizov
  * @version V1.0.0
  * @date    26/11/2014
  * @brief   This file contains global variables and function prototypes for
  * 		 6LowPAN compressor & decompressor
  ******************************************************************************
**/

#ifndef SIXLOWPAN_COMPRESSOR_H_
#define SIXLOWPAN_COMPRESSOR_H_

/* Includes ------------------------------------------------------------------*/
/* Standard includes. */

/* Library includes. */

/* Headers includes. */
#include "misc.h"

/* Macros --------------------------------------------------------------------*/

/* Function Prototypes --------------------------------------------------------*/
bool decompress(byte *, u8, byte *, byte *);
void compress(byte *, bool, byte *);

#endif /* SIXLOWPAN_COMPRESSOR_H_ */
