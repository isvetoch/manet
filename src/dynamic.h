/**
  ******************************************************************************
  * @file    dynamic.h
  * @author  Andrew Gazizov
  * @version V1.0.0
  * @date    18/08/2015
  * @brief   This file contains global variables and function prototypes for
  * 		 dynamic structures
  ******************************************************************************
**/

#ifndef DYNAMIC_H_
#define DYNAMIC_H_

/* Includes ------------------------------------------------------------------*/
/* Standard includes. */

/* Library includes. */

/* Headers includes. */
#include "misc.h"

/* Macros --------------------------------------------------------------------*/

/* Type definitions------------------------------------------------------------*/
typedef struct DictRowStruct *DictRow;
typedef struct DictIteratorStruct *DictIterator;
typedef struct DictHeader *Dict;

typedef struct ListHeader *List;
typedef struct ListIteratorStruct *ListIterator;
typedef struct ListNodeStruct *ListNode;

/* Function Prototypes --------------------------------------------------------*/
Dict Dict_new(void);
void Dict_new_row(Dict, void *, unsigned short, void *, unsigned short);
DictIterator Dict_new_iterator(Dict);
void Dict_delete_iterator(DictIterator);
bool Dict_del_row(Dict, void *, unsigned short);
void *Dict_get(Dict, void *, unsigned short);
void *Dict_iterate_values(DictIterator);


List List_new(void);
bool List_is_empty(List);
void List_append(List, void *, unsigned short);
u8 List_pop(List, void *, unsigned short);
ListIterator List_new_iterator(List);
void List_delete_iterator(ListIterator);
ListNode List_iterate(ListIterator, void *);
void List_del_node(List, ListNode);

//void Dict_test(void);
//void List_test(void);

#endif /* DYNAMIC_H_ */
