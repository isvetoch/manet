/**
  ******************************************************************************
  * @file    misc.h
  * @author  Andrew Gazizov
  * @version V1.0.0
  * @date    23/11/2014
  * @brief   This file contains miscellation global variables and function
  * 			prototypes
  ******************************************************************************
**/

#ifndef MISC_H_
#define MISC_H_

/* Includes ------------------------------------------------------------------*/
/* Standard includes. */
#include <stdio.h>
#include <stdbool.h>
/* Library includes. */

/* Headers includes. */

/* Macros --------------------------------------------------------------------*/
//#define False	0
//#define True 	1
#define None 	0

#define NULL ((void *)0)
#define MIN(a,b) (((a)<(b))?(a):(b))
#define MAX(a,b) (((a)>(b))?(a):(b))

typedef unsigned char byte;
//typedef unsigned char bool;
typedef unsigned char u8;

/* Function Prototypes --------------------------------------------------------*/
void byte_swap(byte *, unsigned short);
void byte_swap_tofrom(byte *, byte *, unsigned short);
bool startswith(byte *, byte *, unsigned short);
void byteinit(byte *);
void byteadd(byte *, byte *);
void byteappend(byte *, byte *, unsigned short);
void valueappend(byte *, byte);
void shortappend(byte *, unsigned short);
void bytecpy(byte *, byte *);
unsigned short bytelen(byte *);
bool bytecomp(byte *, byte *, unsigned short);
bool is_data_empty(byte *);

#endif /* MISC_H_ */
