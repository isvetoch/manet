/**
  ******************************************************************************
  * @file    p802154.h
  * @author  Andrew Gazizov
  * @version V1.0.0
  * @date    29/12/2014
  * @brief   This file contains global variables and function prototypes for
  * 		 802.15.4 packer & unpacker
  ******************************************************************************
**/

#ifndef P802154_H_
#define P802154_H_

/* Includes ------------------------------------------------------------------*/
/* Standard includes. */

/* Library includes. */

/* Headers includes. */
#include "misc.h"
#include <time.h>

/* Macros --------------------------------------------------------------------*/
#define SUCCESS 		0
#define NOT_MY_L2_DST	1
#define DUP_L2_PKT		2
#define FAIL			3

/* Function Prototypes --------------------------------------------------------*/
void L2Master(byte *, u8);
void l2pack(byte *, byte *, byte *, bool, byte *);
u8 l2unpack(byte *);
void L2Neighbors_init(void);
void L2Neighbors_clean(time_t);
u8 P802154(byte *);


// L2 packet form
struct {
	u8 seq;

	byte src[8];
	byte dst[8];
	byte data[256];

	u8 slen;
	u8 dlen;
} p802154;

#endif /* P802154_H_ */
