/**
  ******************************************************************************
  * @file    load.h
  * @author  Andrew Gazizov
  * @version V1.0.0
  * @date    03/12/2014
  * @brief   This file contains global variables and function prototypes for
  * 		 LOAD routing protocol and tables
  ******************************************************************************
**/

#ifndef LOAD_H_
#define LOAD_H_

/* Includes ------------------------------------------------------------------*/
/* Standard includes. */
#include <time.h>

/* Library includes. */

/* Headers includes. */
#include "misc.h"

/* Macros --------------------------------------------------------------------*/

/* Function Prototypes --------------------------------------------------------*/
void Router_init(void);
bool Router_has_route(byte *);
void Router_make_route(byte *);
void Router_clean(time_t);
void load_an(byte *, char);
void Router_error(byte *, byte *);

#endif /* LOAD_H_ */
