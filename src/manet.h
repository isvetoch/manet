/**
  ******************************************************************************
  * @file    node.h
  * @author  Andrew Gazizov
  * @version V1.0.0
  * @date    29/12/2014
  * @brief   This file contains main program configuration
  ******************************************************************************
**/

#ifndef MANET_H_
#define MANET_H_

/* Includes ------------------------------------------------------------------*/
/* Standard includes. */

/* Library includes. */

/* Headers includes. */
#include "misc.h"

/* Macros --------------------------------------------------------------------*/
//#define VERBOSE
#define stm32w	0
#define cc430	1

#define RUN		0
#define SUSPEND 1

/* Global Variables -----------------------------------------------------------*/
struct {
	// Network interface options
	char *iface;		// prefix of tun interface name
	// Serial port options
	char *port;		// serial port file. default: /dev/ttyUSB0
	unsigned int baud;  // baudrate
	// Radio chip options
	char *chip;
	char *antenna;		// type of antenna: external or internal
	u8 power;			// transmit power 0...100, default: max
	u8 channel;			// radio channel 0 .. 15
	bool lan;			// use ethernet LAN network for some radio nodes and interface name of ethernet LAN network
	char *lan_iface;
	bool dbus;
	bool node_status;
	char *log_level;
} cfg;


/* Function Prototypes --------------------------------------------------------*/

#endif /* MANET_H_ */
